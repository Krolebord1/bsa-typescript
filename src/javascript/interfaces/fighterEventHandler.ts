import { IFighter } from "./fighter";

export type FighterEventHandler = (event: Event, fighter: IFighter) => Promise<void>;
