interface ElementData {
  tagName: string,
  className?: string,
  attributes?: Record<string, string>
}

export function createElement({ attributes, className, tagName }: ElementData): HTMLElement {
  const element = document.createElement(tagName);

  if (className) {
    element.classList.add(className);
  }

  if(attributes) {
    Object.keys(attributes).forEach(key => element.setAttribute(key, (attributes)![key]!));
  }

  return element;
}
