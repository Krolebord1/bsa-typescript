import { ApiMethod } from "../constants/apiMethods";
import { API_URL } from "../constants/apiURL";

export async function getFromApi<T>(endpoint: string): Promise<T> {
  const url = API_URL + endpoint;
  const options: { method: ApiMethod }
      = { method: 'GET' };

  let response: Response;

  try {
    response = await fetch(url, options);
  }
  catch (error) {
    return Promise.reject(formattedError(error.toString(), 'getFromApi', endpoint));
  }

  if(!response.ok)
    return Promise.reject(formattedError(`Response code is ${response.status}`, 'getFromApi', endpoint));

  const { content }: { content: string } = await response.json();

  return await JSON.parse(atob(content));
}

function formattedError(reason: string, method: string, endpoint: string): Error {
  return new Error(`Couldn't request api\nReason: ${reason}\nFailed method: ${method}\nEndpoint: ${endpoint}`);
}
