import { getFighters } from './services/fightersService'
import { createFighters } from './fightersView';

export async function startApp(): Promise<void> {
  const rootElement = document.getElementById('root');
  const loadingElement = document.getElementById('loading-overlay');

  if(!rootElement) {
    console.error("Couldn't find element with id=root")
    return;
  }

  if(!loadingElement) {
    console.error("Couldn't find element with id=loading-overlay")
    return;
  }

  try {
    loadingElement.style.visibility = 'visible';
    
    const fighters = await getFighters();
    const fightersElement = createFighters(fighters);

    rootElement.appendChild(fightersElement);
  } catch (error) {
    console.warn(error);
    rootElement.innerText = 'Failed to load data';
  } finally {
    loadingElement.style.visibility = 'hidden';
  }
}
