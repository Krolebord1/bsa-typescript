import { createElement } from './helpers/domHelper';
import { IFighter } from "./interfaces/fighter";
import { FighterEventHandler } from "./interfaces/fighterEventHandler";

export function createFighter(fighter: IFighter, handleClick: FighterEventHandler, selectFighter: FighterEventHandler): HTMLElement {
  const { name, source } = fighter;
  const nameElement = createName(name);
  const imageElement = createImage(source);
  const checkboxElement = createCheckbox();
  const fighterContainer = createElement({ tagName: 'div', className: 'fighter' });
  
  fighterContainer.append(imageElement, nameElement, checkboxElement);

  const preventCheckboxClick = (event: Event) => event.stopPropagation();
  const onCheckboxClick = (event: Event) => selectFighter(event, fighter);
  const onFighterClick = (event: Event) => handleClick(event, fighter);

  fighterContainer.addEventListener('click', onFighterClick, false);
  checkboxElement.addEventListener('change', onCheckboxClick, false);
  checkboxElement.addEventListener('click', preventCheckboxClick , false);

  return fighterContainer;
}

function createName(name: string) {
  const nameElement = createElement({ tagName: 'span', className: 'name' });
  nameElement.innerText = name;

  return nameElement;
}

function createImage(source: string) {
  const attributes = { src: source };

  return createElement({ tagName: 'img', className: 'fighter-image', attributes });
}

function createCheckbox() {
  const label = createElement({ tagName: 'label', className: 'custom-checkbox' });
  const span = createElement({ tagName: 'span', className: 'checkmark' });
  const attributes = { type: 'checkbox' };
  const checkboxElement = createElement({ tagName: 'input', attributes });

  label.append(checkboxElement, span);
  return label;
}
