import { IFighterDetails } from "../interfaces/fighter";
import { showFighterDetailsModal } from "./fighterDetails";

export function showWinnerModal(fighter: IFighterDetails): void {
    showFighterDetailsModal(fighter, 'Winner');
}
