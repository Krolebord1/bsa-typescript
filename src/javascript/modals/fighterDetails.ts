import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { IFighterDetails } from "../interfaces/fighter";

export function showFighterDetailsModal(fighter: IFighterDetails, title: string = 'Fighter info'): void {
  const bodyElement = createFighterDetails(fighter);
  showModal({ title: title, bodyElement });
}

function createFighterDetails(fighter: IFighterDetails): HTMLElement {
  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });

  const imageElement = createFighterImage(fighter);

  const statsElement = createFighterStats(fighter);

  fighterDetails.append(imageElement, statsElement);

  return fighterDetails;
}

function createFighterImage(fighter: IFighterDetails): HTMLElement {
  const { source } = fighter;
  return createElement({ tagName: 'img', className: 'fighter-image', attributes: { src: source }});
}

function createFighterStats(fighter: IFighterDetails): HTMLElement {
  const { name, attack, defense, health } = fighter;

  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  nameElement.innerText = 'Name: ' + name;

  const healthElement = createElement({ tagName: 'span', className: 'fighter-health'});
  healthElement.innerText = 'Health: ' + +health.toFixed(2);

  const defenseElement = createElement({ tagName: 'span', className: 'fighter-defense'});
  defenseElement.innerText = 'Defense: ' + defense;

  const attackElement = createElement({ tagName: 'span', className: 'fighter-attack'});
  attackElement.innerText = 'Attack: ' + attack;

  const statsElement = createElement( { tagName: 'div', className: 'fighter-stats'});
  statsElement.append(
      nameElement,
      healthElement,
      defenseElement,
      attackElement
  );

  return statsElement
}
