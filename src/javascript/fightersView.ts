import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { IFighter, IFighterDetails } from "./interfaces/fighter";
import { getFighterDetails } from "./services/fightersService";
import { FighterEventHandler } from "./interfaces/fighterEventHandler";

export function createFighters(fighters: IFighter[]): HTMLElement {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

async function showFighterDetails(_event: Event, fighter: IFighter): Promise<void> {
  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: string): Promise<IFighterDetails> {
  return await getFighterDetails(fighterId);
}

function createFightersSelector(): FighterEventHandler {
  const selectedFighterIds: Set<string> = new Set<string>();

  return async function selectFighterForBattle(event: Event, fighter: IFighter): Promise<void> {
    if ((event.target as HTMLInputElement).checked) {
      selectedFighterIds.add(fighter._id);
    } else {
      selectedFighterIds.delete(fighter._id);
    }

    if (selectedFighterIds.size === 2) {
      const idsIterator = selectedFighterIds.values();
      const loadFighterPromises: [Promise<IFighterDetails>, Promise<IFighterDetails>] = [
        getFighterInfo(idsIterator.next().value),
        getFighterInfo(idsIterator.next().value)
      ];

      const [firstFighter, secondFighter] = await Promise.all(loadFighterPromises);

      const winner = fight(firstFighter, secondFighter);
      showWinnerModal(winner);
    }
  }
}
