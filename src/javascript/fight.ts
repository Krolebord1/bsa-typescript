import { IFighterDetails } from "./interfaces/fighter";

export function fight(firstFighter: IFighterDetails, secondFighter: IFighterDetails): IFighterDetails {
    while (true) {
        if(firstFighter.health <= 0)
            return secondFighter;

        applyAttack(firstFighter, secondFighter);

        if(secondFighter.health <= 0)
            return firstFighter;

        applyAttack(secondFighter, firstFighter);
    }
}

function applyAttack(attacker: IFighterDetails, enemy: IFighterDetails): void {
    enemy.health -= getDamage(attacker, enemy);

    if(enemy.health < 0)
        enemy.health = 0;
}

export function getDamage(attacker: IFighterDetails, enemy: IFighterDetails): number {
    return Math.max(getHitPower(attacker) - getBlockPower(enemy), 0);
}

export function getHitPower(fighter: IFighterDetails): number {
    const criticalHitChance = 1 + Math.random();
    return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter: IFighterDetails): number {
    const dodgeChance = 1 + Math.random();
    return fighter.defense * dodgeChance;
}
