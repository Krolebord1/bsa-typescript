import { getFromApi } from '../helpers/apiHelper';
import { IFighter, IFighterDetails } from "../interfaces/fighter";
import { GetFighterByIdEndpoint, GetFightersEndpoint } from "../constants/apiURL";
import { MockFighters, MockFightersDetails } from "../helpers/mockData";

const useMockAPI = false;
const mockApiDelay = 1000;

export async function getFighters(): Promise<IFighter[]> {
  if(useMockAPI)
    return new Promise((resolve) => {
      setTimeout(() => resolve(MockFighters), mockApiDelay);
    });

  const endpoint = GetFightersEndpoint();
  return await getFromApi<IFighter[]>(endpoint);
}

export async function getFighterDetails(id: string): Promise<IFighterDetails> {
  if(useMockAPI) {
    const fighterDetails = MockFightersDetails.find((fighter) => fighter._id === id);

    if(!fighterDetails)
      throw new Error(`Couldn't load fighter id=${id}`);

    return new Promise((resolve) => {
      setTimeout(() => resolve(fighterDetails), mockApiDelay);
    });
  }

  const endpoint = GetFighterByIdEndpoint(id);
  return await getFromApi<IFighterDetails>(endpoint);
}

